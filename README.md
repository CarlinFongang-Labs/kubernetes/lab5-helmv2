# Kubernetes | Helm v3 et chart
_______


><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang"> 

> **Carlin FONGANG**  | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

## Contexte
Ce laboratoire vise à présenter les étapes de mise en place d'un volume persistant sur Kubernetes.

## Objectifs

Dans ce laboratoire, nous allons :

1. Installer Helm version 3 pour faciliter la gestion des applications Kubernetes.
2. Déployer le système de gestion de contenu (CMS) WordPress en utilisant un chart Helm spécifique à WordPress.
3. Exposer l'application à l'extérieur via un service NodePort configuré pour HTTP.
4. Personnaliser les configurations par défaut en utilisant un fichier **"values.yml"**. Dans ce fichier, nous modifierons des paramètres tels que le nom d'utilisateur et le mot de passe pour l'administration du site WordPress. 


## 1. C'est quoi Helm ?
Helm est un gestionnaire de paquets pour Kubernetes. Il permet aux utilisateurs de définir, installer et mettre à jour des applications Kubernetes de manière organisée. Helm utilise des packages appelés "charts", qui contiennent toutes les ressources nécessaires pour déployer une application, un service ou un ensemble de fonctionnalités sur Kubernetes. Ces charts aident à gérer les dépendances entre les services et à configurer les applications Kubernetes de manière répétable et standardisée. Helm simplifie donc le déploiement et la gestion des applications sur Kubernetes en automatisant plusieurs tâches complexes.


## 2. **Prérequis** : Liste des exigences matérielles et logicielles.

Dans notre cas, nous allons provisionner une instances EC2 s'exécutant sous Ubuntu 20.04 Focal Fossa LTS, grace au provider AWS, à partir delaquelle nous effectuerons toutes nos opérations.

[Provisionner une instance EC2 sur AWS à l'aide de Terraform](https://gitlab.com/CarlinFongang-Labs/Terraform/lab2-terraform-aws) (recommandé)

[Provisionner une instance EC2 sur AWS à l'aide d'Ansible](https://gitlab.com/CarlinFongang-Labs/Ansible/lab10-deploy-ec2)


## 2. Création d'un cluster

Consultez le document [Installer kubeadm](https://gitlab.com/CarlinFongang-Labs/kubernetes/lab1.1-install-kubeadm.git)

## 3. **Installation de Helm v3**

````bash
curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3
````

````bash
chmod 600 get_helm.sh
````

````bash
 bash get_helm.sh
````
>![alt text](img/image.png)


## 4. Ajout de depo du chart
````bash
helm repo add bitnami https://charts.bitnami.com/bitnami
````

## 6. **Personnalisation du fichier values.yml**

Pour redefinir les variables du deploiement wordpress et afin de l'adapter à nos besoin, nous allons nous reférer au manifest yaml par defaut [VALUE](https://github.com/bitnami/charts/blob/main/bitnami/wordpress/values.yaml)

````bash
sudo nano values.yml
````

````yaml
wordpressUsername: admin
wordpressPassword: password
service:
  type: NodePort
  nodePorts:
    http: "30000"
persistence:
  enabled: false
mariadb:
  primary:
    persistence:
      enabled: false
````

Ce manifest fourni les configuration utiles à l'instance WordPress avec des paramètres spécifiques, permettant ainsi de surchargé les variables par défaut définis dans le chart bitnami pour wodpress. Pour avoir connaissance des ces variables, visiter le site [wordpress chart](https://github.com/bitnami/charts/blob/main/bitnami/wordpress/README.md)

## 7. Déploiement de wordpress

````bash
helm install wordpress bitnami/wordpress -f values.yml
````

>![alt text](img/image-1.png)

>![alt text](img/image-2.png)
*Les pods wordpress et mariadb sont bien déployés*


## 7. **Vérification et tests**
Pour vérifier la disponiblité de notre application wordpress, nous allons entrée le l'ip de l'un des nodes du cluster suivi du port 3000 dans un navigateur.

````bash
http://0df76569711c.mylabserver.com:30000/
````

>![alt text](img/image-3.png)
*l'acceuil est disponible*

````bash
http://0df76569711c.mylabserver.com:30000/admin
````
>![alt text](img/image-4.png)
*Page de connexion à l'administration*

>![alt text](img/image-5.png)
*Dashbord d'administration wordpress*

## Documentation 

### [Installer kubeadm sur ubuntu](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/)

### [Creating a Single Control-Plane Cluster with kubeadm](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/create-cluster-kubeadm/)
